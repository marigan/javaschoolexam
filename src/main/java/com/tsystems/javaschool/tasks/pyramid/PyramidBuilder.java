package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {
    // key - sum of the count of elements, value - count of elements in the row
    private final Map<Integer,Integer> requiredNumbersCount;

    {
        requiredNumbersCount = new HashMap<>();
        int elementsCountSum = 3;
        int elementsInRow = 2;

        while (elementsCountSum < Integer.MAX_VALUE - 1 && elementsCountSum > 0) {
            requiredNumbersCount.put(elementsCountSum, elementsInRow);
            elementsInRow++;
            elementsCountSum += elementsInRow;
        }
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int result[][];
        int listLength = inputNumbers.size();

        if (inputNumbers.contains(null) || !requiredNumbersCount.containsKey(listLength)) {
            throw new CannotBuildPyramidException();
        } else {
            int numbersCountInLastRow = requiredNumbersCount.get(listLength);
            int rowCount = numbersCountInLastRow;
            int columnCount = numbersCountInLastRow + (numbersCountInLastRow - 1);
            result = new int[rowCount][columnCount];

            Collections.sort(inputNumbers);
            int zeroesOnSides = numbersCountInLastRow - 1;
            int index = 0;

            for (int i = 0; i < rowCount; i++) {
                for (int j = zeroesOnSides; j < columnCount - zeroesOnSides; j += 2) {
                    result[i][j] = inputNumbers.get(index);
                    index++;
                }
                zeroesOnSides -= 1;
            }
        }

        return result;
    }
}