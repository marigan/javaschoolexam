package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {
    private int linksCount;
    private int[][] graph; // adjacency matrix for requested zones
    private boolean[] used; // array of labels for searching links between requested zones

    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        if (zoneState == null || requestedZoneIds == null) return false;
        if (requestedZoneIds.size() < 2) return false;

        int requestedZonesCount = requestedZoneIds.size();
        graph = new int[requestedZonesCount][requestedZonesCount];
        used = new boolean[requestedZonesCount];
        // key - zone ID, value - index in adjacency matrix
        Map<Integer, Integer> zonesID = new HashMap<>();
        for (int i = 0; i < requestedZonesCount; i++) {
            zonesID.put(requestedZoneIds.get(i), i);
        }

        // fill adjacency matrix only for requested zones
        for (int i = 0; i < zoneState.size(); i++) {
            if (zonesID.containsKey(zoneState.get(i).getId())) {
                Zone zone = zoneState.get(i);
                List<Integer> neighbours = zone.getNeighbours();
                int currentZoneIndex = zonesID.get(zone.getId());

                for (int j = 0; j < neighbours.size(); j++) {
                    int neighbouringZoneID = neighbours.get(j);

                    if (zonesID.containsKey(neighbouringZoneID)) {
                        int neighbourZoneIndex = zonesID.get(neighbouringZoneID);
                        graph[currentZoneIndex][neighbourZoneIndex] = 1;
                        graph[neighbourZoneIndex][currentZoneIndex] = 1;
                    }
                }
            }
        }

        findNeighbour(zonesID.get(requestedZoneIds.get(0)), requestedZonesCount);

        // if requested zones are connected - return true
        return linksCount == requestedZonesCount - 1;
    }

    private void findNeighbour(int currentZoneIndex, int zonesCount) {
        used[currentZoneIndex] = true;

        for (int neighbourIndex = 0; neighbourIndex < zonesCount; neighbourIndex++)
            if (!used[neighbourIndex] && graph[currentZoneIndex][neighbourIndex] == 1) {
                linksCount++;
                findNeighbour(neighbourIndex, zonesCount);
            }
    }
}
